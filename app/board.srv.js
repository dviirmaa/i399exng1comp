(function () {
    'use strict';

    angular.module('app').service('board', Srv);

    Srv.$inject = ['$q'];

    function Srv($q) {

        this.getBoardState = getBoardState;
        this.setLight = setLight;
        this.setAlarm = setAlarm;

        function getBoardState() {
            return { light: false, alarm: true };
        }

        function setLight(isOn) {
        }

        function setAlarm(isOn) {
        }

    }


})();
